import React from 'react'
import { useGlobalContext } from './context';
import {MdOutlineKeyboardArrowUp, MdOutlineKeyboardArrowDown} from 'react-icons/md'
import './App.css';
const CartItem = ({ id, img, title, price, amount }) => {
  return (
    <article className='cart-item'>
      <img src={img} alt={title} />
      <div>
        <h4>{title}</h4>
        <h4 className='item-price'>${price}</h4>
        {/* remove button */}
        <button
          className='remove-btn'
          onClick={() => console.log('remove item')}
        >
          remove
        </button>
      </div>
      <div>
        {/* increase amount */}
        <button className='amount-btn' onClick={() => console.log('increase')}>
          <MdOutlineKeyboardArrowUp/>
        </button>
        {/* amount */}
        <p className='amount'>{amount}</p>
        {/* decrease amount */}
        <button className='amount-btn' onClick={() => console.log('decrease')}>
          <MdOutlineKeyboardArrowDown/>
        </button>
      </div>
    </article>
  )
}

export default CartItem