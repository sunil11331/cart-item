import React from 'react'
import { useGlobalContext } from './context'
import './App.css';
import {BsHandbagFill} from 'react-icons/bs'

const Navbar = () => {
  const {amount} = useGlobalContext()
  return (
    <nav>
      <div className='nav-center'>
        <h3>Mobile World</h3>
        <div className='nav-container'>
          <BsHandbagFill id='aaa'/>
          <div className='amount-container'>
            <p className='total-amount'>{amount}</p>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar